# CHANGELOG

- 20210521

    		Add baud rate detection for remote rigs

    		Correct propCIVAddr to work if less than 0xe0

    		Change audiohandler to use latency for tx audio


- 20210520

    		Added IC-756 Pro. Tested UI and back-end response with 7300 and fake
    		RigID reply.

    		Added additional support for the IC-756 Pro III. Essentially untested.

    		Cleaned up warning and UI help text.

    		Add more features to rigstate

- 20210519

    		Model ID text format fixed. Shows IC-0x followed by the raw rig ID
    		received.

    		Fixed issue where unknown rigs were identified as 0xff. All rigs are now
    		identified using rigCaps.modelID, populated with the raw response from
    		the Rig ID query. Enumerations of known rig types continue to fall into
    		rigCaps.model, and unknwn rigs will match to rigCaps.model=modelUnknown,
    		as before.

    		Serial baud rate is in the UI now. Added some enable/disable code to
    		prevent confusion about which options can be used with which types of
    		connections.

    		Better about box.

    		fixed filename in instructions

		Add MacOS serial port entitlement


- 20210518

    		Remove unused variables

    		Make windows build include git version
    		Command line version of git must be available in the path.

    		Various file tidying for Windows/Mac builds

    		Flush buffers if too many lost packets.

    		remove duplicate audioPacket metatype

    		Fix silly error in udpserver audio

    		Allow receive only in udpHandler

    		Manual CIV is now read from the preferences and populates the UI
    		accordingly.

    		Changed UI a little, and added manual CI-V options. Seems to work well.

- 20210517

    		Fixes for MacOS build - sandbox

    		Fixes for mac logging/pty since adding sandbox

    		Make audio input buffer a qMap

    		Use qMap instead of qVector for buffers as they are auto-sorted.

- 20210516

    		Fixed attenuator on IC-7000

    		register audio metatype in wfmain with all of the others

    		Move manual serial port so linux/mac only

- 20210516 	Added IC-7200 support. This has not been tested.

- 20210515	Change to correct bug when CI-V is user-specified and rigCaps were never
    		populated.

    		Making the s-meter error command easier to read.

    		Added IC-706 to rigCaps. Support for the IC-706 is currently broken but
    		development is in progress.

    		Added check for if the rig has spectrum during initial state queries.

    		BSR debug code.

		Small fixes for rigctld

- 20210514	Make UDP port textboxes expanding.

    		Selecting an antenna now sets that antenna for TX and RX.
                
    		wfview now closes when the main window is closed.
                
    		Filter selection now checks for data mode.
                
    		Preliminary IC-7000 support. TODO: Input selection, modes, filters,
    		reported bug with frequency input and s-meter.
                
    		Moved the power buttons.

    		Cyan for the tuning line.

    		Resize UDP port text boxes (again!)

    		ake UDP port textboxes expanding.

    		Fixes to UDP server

		Hopefully improve stability of pty by filtering traffic for any other CIV id.

- 20210513	Additional search path for macports compatibility (macOS only).

    		Slower polling for older rigs using lower serial baud.

    		Fix CI-V packet length bug in udphandler

    		Set pty serial port to NULL by default

- 20210512	Fix for crash on MacOS

- 20210511	Fixes for virtual serial port in Windows.

    		Initial commit of pty rewrite
		There is a new combobox in Settings which shows the file that the pty device is mapped to. 
		You can select from ~/rig-pty[1-8] which should work if you have multiple instances of wfview.

- 20210509	Enhanced accessibility for the UI.

- 20210508	Data Mode now sends the currently selected filter.

    		Removed -march=native compiler and linker flags. This should make the
    		binary builds more compatible, and also eliminate an issue on Pop! OS,
    		where the default optimizations clashed.

		Preliminary IC910H support

- 20210507	Added serial port iterators for the IC-7610 and IC-R8600. Untested.

    		removing debug info we didn't need.

    		Adding /dev/ to discovered serial devices, also ignoring case on "auto"
    		for serial port device.

    		wfview's own memory system now tracks mode correctly. *however*, it
    		needs work:
   		It should track the selected filter, since this information is generally
    		available and useful, and it should also be storing the frequencies in Hz. 
		I am also not sure how well the stored memory mode specification
    		will work across multiple rigs.

    		Added more mode shortcuts: A = airband, $ = 4 meter band (shift-4), W =
    		WFM, V = 2M, U = 70cm, Shift-S = 23cm (S-band)

    		Fixed BSR mode and filter selection.

    		The band stacking register now uses the newer integer frequency parsing.
    		We also communicate a little slower to the serial rigs, which seems more
    		reliable.

    		Updater serialDeviceListCombo with current COM/tty port setting

    		pttyHandler doesn't use pty parameter in Linux so set to Q_UNUSED to eliminate warning


- 20210506	Force DTR/RTS to false to stop rigs going into TX if USB Send is enabled

    		Convert project to 32bit default on Windows and remove beginnings of VSPE code.


- 20210505	Add code to select virtual serial port

    		fixed a typo in mint instructions and ubuntu instructions

- 20210504	fixed the display instead of rigcaps so that ant sel starts with 1 instead of 0

    		Fix to add a blank user line in server if no users are configured.

    		small changes where to find the release; removed the src/build target directory requirement as the release unpacks in ./dist

- 20310503	Fix for Built-in audio on MacOS

- 20210501	Fixed bug 007, which allowed negative frequencies to be dialed.

    		Double-clicking the waterfall now obeys the tuning step and rounding
    		option in the settings.

    		Unified frequency-type MHz to use same code as Hz.

    		Add WFM mode for IC705 and remove duplicate WFM mode for IC-R8600

    		make bandType start at 0 so it doesn't overflow rigCaps.bsr

    		Fix windows release build

    		Changed the organization and domain to wfview and wfview.org.

    		Added more modes for the IC-R8600.

- 20210430	Different timing on command polling for serial rigs.


- 20210427	Minor changes to hide sat button until we have time to work on that
    		feature.

    		Additional bands added, Airband, WFM

    		added 630/2200m for 705; probably like the 7300 hardly useable for TX though. Also added auomatic switching to the view pane after non-BSR bands are selected

    		added 630/2200m to 7300/7610/785x

    		derp: fixed main dial freq display for non-BSR 4m band; see also previous commit

    		fixed main dial freq display for non-BSR bands 60, 630, 2200m if such a band was selected in the band select menu

    		Minor change to set frequency, which was lacking "emit" at the start.

    		changed the modeSelectCombo-addItem sequence a bit to have modes grouped; CW and CW-R are now grouped, as well as RTTY/RTTY-R; AM and FM are grouped now

- 20210426	Well, that was fun. Color preferences now work, and colors can be
    		modified from the preference file for custom plot colors.
    
    		The preference file now stores colors as unsigned int. To convert the
    		colors in the file to standard AARRGGBB format (AA = Alpha channel), use
    		python's hex() function. Maybe one day we will fix the qt bug and make
    		this save in a better format.

    		Added dynamic band buttons. Fixed multiple bugs related to various
    		differences in band stacking register addresses (for example, the GEN
    		band on the 705 has a different address from the 7100 and the 7300).
    		This code was only tested with the 9700.

    		started rough docs for the usermanual

- 20210425	More work on rigctld

		Faster polling. Clarified in comments around line 309 in wfmain.cpp.

    		Added ability to read RIT status. Added RIT to initial rig query. Added

    		Added ability to read RIT status. Added RIT to initial rig query. Added
    		variables to handle delayed command timing values. Fixed bug in
    		frequency parsing code that had existed for some time. Changed tic marks
    		on RIT knob because I wanted more of them. Bumped startup initial rig
    		state queries to 100ms. May consider faster queries on startup since we
    		are going to need more things queried on startup.

- 20210424	Receiver Incremental Tuning is in. The UI does not check the rig's
    		initial state yet, but the functions are partially in rigCommander for
    		that purpose.


- 20210423	Found two missing defaults in switch cases inside rigcommander.cpp.

		Modified rig power management to stop command ques (s-meter and others).
		Upon rig power up, the command queue is repurposed as a 3 second delay
		for bootup, and then, commands are issued to restore the spectrum
		display (if the wf checkbox was checked). I've made new functions,
		powerRigOff and powerRigOn, for these purposes.

    		work in progress on spectrum enable after power off/on.

		powerOff should work now.

- 20210420 	rigctl working (sort of) with WSJT-X

- 20210419	Initial commit of rigctld (doesn't currently do anything useful!)


- 20210417	Adding a default position for the frequency indicator line.

    		Goodbye tracer


- 20210416	added support info for prebuild-systems


- 20210412	added airband, dPMR, lw/mw European and fast HF/70/23cm 1 MHz tuning steps


- 20210411	Added ATU feature on 7610.

    		Now grabs scope state on startup. Found bug, did not fix, in the
    		frequency parsing code. Worked aroud it by using doubles for now.


- 20210410	Preamp and attenuator are now queried on startup. Additionally, the
    		preamp is checked when the attenuator is changed, and the attenuator is
    		checked with the preamp is changed. This helps the user understand if
    		the rig allows only one or the other to be active (such as the IC-9700).

    		Added frequency line. Let's see how we like it.

    		Add some preliminary parts of getting the attenuator, preamp, and
   		antenna selection on startup. UI not updated yet but getting there.


- 20210409	Moved ATU controls to main tab.

		Added waterfall theme combo box

    		Removed buttons from attenuator preamp UI controls.

    		Antenna selection might work, untested.

    		Preamp code is in. Can't read the current preamp state yet but we can
    		set it. Nicer names for preamp and attenuator menu items.


- 20210408    	Preamp data is now tracked (but not used yet)
		re-added lost tooptip for SQ slider


- 20210407 	Minor disconnect in the getDTCS call

    		Attenuators are in! Please try them out!


- 20210406 	The repeater setup now disables elements for things your rig doesn't do.

    		We now query the repeater-related things on startup, such that the
    		repeater UI is populated correctly.

 	   	We now have kHz as the assumed input format if there isn't a dot in the
    		entry box. Enjoy that!

    		Minor change so that we track the selected tone to mode changes, keeping
    		the radio in sync with the UI.

    		Tone, Tone Squelch, and D(T)CS seem to work as expected. Mode can be
    		selected.


- 20210405	removed 150 Hz CTCSS / NATO as it can't make that by itself

    		added 77.0 Hz tone to CTCSS

    		We can now read the repeater access mode and update the UI. What remains
    		is to be able to set the mode.

    		Working read/write of Tone, TSQL, and DTCS tones/code. Some code is also
    		present now to change the mode being used (tone, tsql, DTCS, or some
    		combo of the two).


- 20210404	Tone, TSQL, and DTCS code added, but not complete.

    		better tone mode names

    		Started work on the tone mode interface.


- 20210401	Added placeholders for attenuator, preamp, and antenna selection UI
    		controls.

    		Moved some repeater-related things out from rig commander and into a
    		shared header.


- 20210331	Adjusting signals and slots for repeater duplex.

    		Basic duplex code copied from wfmain to the new repeatersetup window.


- 20210330	Added conditional to debug on serial data write size.


- 20210329	Fix crash when radio is shutdown while wfview is connected.


- 20210311      Add local volume control for UDP connections.

                add volume control to audiohandler

                Small fixes to UDP server

                Add USB audio handling to UDP server

                Changed frequency parameters to (mostly) unsigned 64-bit ints. This
                makes all the rounding code very simple and removes many annoying lines
                of code designed to handle errors induced by using doubles for the
                frequency.

- 20210310	audio resampling added / opus

		updates on virtual serial port 

		fixed input combo boxes from growing

		fixed 4:15 rollover/disconnect issue		

- 20210304	Tuning steps! Still need to zero out those lower digits while scrolling
    		if box is checked.

    		Fix spectrum peaks as well.

		Fix spectrum 'flattening' with strong signal

		Add separate mutex for udp/buffers.

		supported rigs IC705, IC7300 (over USB, no sound), IC7610, IC785x

- 20210227	changed the way udp audio traffic is handled
                on both the TX and RX side.

- 20210226	Fixed minor bug where flock did not stop double-clicking on the
    		spectrum.

                S-meter ballistics:
                Turned up the speed. Once you see fast meters, there's no going back.
		Tested at 5ms without any issues, comitting at 10ms for now. Note that
    		50ms in the first 'fixed' meter code is the same as 25ms now due to how
    		the command queue is structured. So 10ms is only a bit faster than
    		before.
                Fixed meter polling issue

- 20210224      fixed 785x lockup
                Added scale and dampening/averaging to s-meter

- 20210222      f-lock works now

- 20210221	added working s-meter
		added working power meter
		added visual studio 2019 solution (windows builds)
		changed packet handling, WIP
		rigtype stays in view on status bar
		added audio input gain slider

- 20210218
		added SQ slider
		added TX power slider
		added TX button
		added simplex/duplex/auto shifts
		added debug logging
		added TX and RX codec selections
		added RX audio buffer size slider
		started adding server setup
		
- 20210210	added control over LAN for rigs that support it
                has been tested on the 705, 7610, 785x, 9700.
		should work on 7700, 7800, other rigs.
		added different sampling rates for rx
		added different codecs
		added scope data for 7610, 785x.  
		several cosmetic changes in the UI
		fixed a slew of bugs
		builds on MAC-OS, Linux, Windows
		added ToFixed
		implemented connect/disconnect rig
		improved USB serial code
		fixed memory leak 
		redesigned rx audio path for better performance
		added round trip time for network connected devices
		added detection code of rigtype
		automatic detection of CIV control address
		added logfile capability
		works on a raspberry pi (USB and ethernet)


			

            
